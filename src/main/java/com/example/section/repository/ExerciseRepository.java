package com.example.section.repository;

import com.example.section.entity.Exercise;
import com.example.section.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Set;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    @Query("SELECT e FROM Exercise e where e.group.id = :id and e.date >= current date")
    Set<Exercise> findAllExercises(@Param("id") Long id);

    @Query("SELECT e FROM Exercise e where e.date > current date ")
    Set<Exercise> findExerciseByDate();

    Exercise findExerciseByGroupAndDate(Group group, LocalDateTime date);
}
