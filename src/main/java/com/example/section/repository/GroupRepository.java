package com.example.section.repository;

import com.example.section.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {
    public Group findTopByOrderByIdDesc();
}
