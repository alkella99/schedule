package com.example.section.repository;

import com.example.section.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Set<Student> getAllByInGroupIsFalse();
}
