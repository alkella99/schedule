package com.example.section.annotation;

import com.example.section.annotation.validator.ExerciseDateValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ExerciseDateValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExerciseDateValidation {
    String message() default "Некорректный день для занятия, должен быть с ПН по СБ.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
