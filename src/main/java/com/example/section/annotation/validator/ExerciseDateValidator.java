package com.example.section.annotation.validator;

import com.example.section.annotation.ExerciseDateValidation;
import com.example.section.exception.BadRequestException;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDateTime;
import java.time.temporal.ValueRange;

import static java.time.LocalDateTime.now;
/**
 * Класс аннотации для валидации даты нового занятия
 */
public class ExerciseDateValidator implements ConstraintValidator<ExerciseDateValidation, LocalDateTime> {
    @Override
    public boolean isValid(LocalDateTime exerciseDate, ConstraintValidatorContext constraintValidatorContext) throws RuntimeException{
        ValueRange dateRange = ValueRange.of(1, 6);
        if (!dateRange.isValidValue(exerciseDate.getDayOfWeek().getValue())){
            throw new BadRequestException("Некорректный день для занятия, должен быть с ПН по СБ.");
        }
        if (exerciseDate.isBefore(LocalDateTime.now()) || exerciseDate.isEqual(LocalDateTime.now())){
            throw new BadRequestException("Дата нового занятия не может быть равна или ранее текущей даты");
        }
        return dateRange.isValidValue(exerciseDate.getDayOfWeek().getValue());
    }

    @Override
    public void initialize(ExerciseDateValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
}
