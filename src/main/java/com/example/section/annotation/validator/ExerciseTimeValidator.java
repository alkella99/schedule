package com.example.section.annotation.validator;

import com.example.section.annotation.ExerciseTimeValidation;
import com.example.section.exception.BadRequestException;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDateTime;
import java.time.temporal.ValueRange;
/**
 * Класс для валидации временеи нового занятия.
 */
public class ExerciseTimeValidator implements ConstraintValidator<ExerciseTimeValidation, LocalDateTime> {
    @Override
    public boolean isValid(LocalDateTime exerciseDate, ConstraintValidatorContext constraintValidatorContext)
            throws RuntimeException{
        ValueRange hourRange = ValueRange.of(15, 19);
        if (!hourRange.isValidValue(exerciseDate.getHour())){
            throw new BadRequestException("Некорректный время для занятия, должно быть с 15 по 19 часов.");
        }
        return hourRange.isValidValue(exerciseDate.getHour());
    }

    @Override
    public void initialize(ExerciseTimeValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
}
