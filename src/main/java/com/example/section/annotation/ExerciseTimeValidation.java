package com.example.section.annotation;

import com.example.section.annotation.validator.ExerciseTimeValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ExerciseTimeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExerciseTimeValidation {
    String message() default "Некорректный время для занятия, должно быть с 15 по 19 часов.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
