package com.example.section.exception;

public class NotFoundRequestException extends RuntimeException{
    public NotFoundRequestException(String message){
        super(message);
    }
}
