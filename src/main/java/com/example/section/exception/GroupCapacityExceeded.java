package com.example.section.exception;

public class GroupCapacityExceeded extends RuntimeException{
    public GroupCapacityExceeded(String message){
        super(message);
    }
}
