package com.example.section.controller;

import com.example.section.dto.request.GroupRqDto;
import com.example.section.dto.request.StudentsSetRqDto;
import com.example.section.dto.response.GroupRsDto;
import com.example.section.service.GroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер, предназначенный для обработанных запросов ,связанных с группами.
 */
@RestController
public class GroupController {
    @Autowired
    private GroupService groupService;

    /**
     * Метод для создания новой группы
     * @param groupRqDto DTO для передачи параметров от клиента через класс GroupRqDto
     * @see GroupRqDto
     * @return groupRsDto сущность для передачи параметров клиенту через класс GroupRsDto
     * @throws com.example.section.exception.BadRequestException
     */
    @Operation(summary = "Добавление новой группы")
    @PostMapping("/group")
    public GroupRsDto addGroup(@RequestBody GroupRqDto groupRqDto) throws RuntimeException{
        return groupService.addGroup(groupRqDto);
    }

    /**
     * Метод для добавления студентов в группы
     * @param studentsSetRqDto DTO для передачи параметров от клиента через класс StudentsListRqDto
     * @see StudentsSetRqDto
     * @return groupRsDto сущность для передачи параметров клиенту через класс GroupRsDto
     * @throws com.example.section.exception.BadRequestException
     */
    @Operation(summary = "Добавление студента в группу")
    @PutMapping("/group/{id}")
    public GroupRsDto addStudentsToGroup(@RequestBody StudentsSetRqDto studentsSetRqDto,
                                         @Parameter(description = "ID группы")
                                         @PathVariable Long id) throws Exception{
        return groupService.addStudentsToGroup(studentsSetRqDto, id);
    }

    /**
     *Метод для поиска группы по ID
     * @param id - ID группы
     * @return GroupRsDto - DTO группы для отправки клиенту
     * @see GroupRsDto
     * @throws com.example.section.exception.BadRequestException
     */
    @Operation(summary = "Поиск группы по ID")
    @GetMapping("/group/{id}")
    public GroupRsDto getGroupById(@Parameter(description = "ID группы")
                                       @PathVariable Long id) throws Exception{
        return groupService.getGroupById(id);
    }
}
