package com.example.section.controller;

import com.example.section.dto.request.AbsentStudentSetRqDto;
import com.example.section.dto.request.ExerciseRqDto;
import com.example.section.dto.request.PresenceRqDto;
import com.example.section.dto.response.AbsentStudentSetRsDto;
import com.example.section.dto.response.ExerciseRsDto;
import com.example.section.service.ExerciseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Класс контроллера обработки запросов связанных с сущностью Exercise.
 */
@RestController
public class ExerciseController {
    @Autowired
    private ExerciseService exerciseService;

    /**
     *Метод для добавления нового занятия.
     * @param exerciseRqDto ExerciseRqDto, сущность DTO для передачи данных.
     * @see ExerciseRqDto
     * @return возвращает ExerciseRsDto, сущность DTO для отправки данных пользователю.
     * @see ExerciseRsDto
     * @throws RuntimeException
     * @see com.example.section.exception.BadRequestException
     */
    @Operation(summary = "Добавление занятия")
    @PostMapping("/exercise")
    public ExerciseRsDto addExercise(@Valid @RequestBody ExerciseRqDto exerciseRqDto) throws Exception{
        return exerciseService.addExercise(exerciseRqDto);
    }

    /**
     * Метод для внесения отсутствующих на занятии
     * @param absentStudentSetRqDto предназначен для передачи списка отсутствующих и даты проведения занятия.
     * @see PresenceRqDto
     * @param exercise_id - id занятия.
     * @return ExerciseRsDto DTO для передачи данных клиенту.
     * @see ExerciseRsDto
     * @throws RuntimeException
     * @see com.example.section.exception.BadRequestException
     */

    @Operation(summary = "Внесение остутствующих на занятии")
    @PutMapping("/exercise/{exercise_id}")
    public ExerciseRsDto setAbsent(@RequestBody AbsentStudentSetRqDto absentStudentSetRqDto,
                                   @Parameter(description = "ID занятия")
                                   @PathVariable Long exercise_id) throws Exception{
        return exerciseService.setAbsent(absentStudentSetRqDto, exercise_id);
    }

    /**
     * Метод для получения списка отсутствующих на занятии
     * @param exercise_id ID занятия
     * @return AbsentStudentSetRsDto, который содержит ID и дату занятия
     * @see AbsentStudentSetRsDto
     * @throws Exception выбрасывает исключение, если не найдена группа по ID
     */
    @Operation(summary = "Запрос остутствующих на занятии")
    @GetMapping("/exercise/{exercise_id}")
    public AbsentStudentSetRsDto getAbsent(@PathVariable Long exercise_id) throws Exception{
        return exerciseService.getAbsent(exercise_id);
    }
}
