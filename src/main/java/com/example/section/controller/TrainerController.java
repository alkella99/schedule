package com.example.section.controller;

import com.example.section.dto.request.TrainerRqDto;
import com.example.section.dto.response.TrainerRsDto;
import com.example.section.service.TrainerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Класс контроллера для обработки запросов, связанных с сущностью Trainer.
 */
@RestController
public class TrainerController {
    @Autowired
    private TrainerService trainerService;

    /**
     * Метод для добавления нового тренера.
     * @param trainerRqDto предназначен для передачи данных трененра через DTO TrainerRqDto.
     * @see TrainerRqDto
     * @return TrainerRsDto предназначен для отправки данных клиенту.
     * @see TrainerRsDto
     */
    @Operation(summary = "Добавление нового тренера")
    @PostMapping("/trainer")
    public TrainerRsDto addTrainer(@RequestBody TrainerRqDto trainerRqDto){
        return trainerService.addTrainer(trainerRqDto);
    }
}
