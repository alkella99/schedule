package com.example.section.controller;

import com.example.section.dto.request.StudentRqDto;
import com.example.section.dto.response.ExerciseRsDto;
import com.example.section.dto.response.StudentRsDto;
import com.example.section.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Класс контроллера для обработки запросов, связанных с сущностью Student.
 */
@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * Метод для добавления нового ученика.
     * @param studentRqDto предназначен для передачи данных об ученике через StudentRqDto.
     * @see StudentRqDto
     * @return StudentRsDto - предназначен для предоставлении данных клиенту.
     * @see StudentRsDto
     */
    @Operation(summary = "Внесение нового студента")
    @PostMapping("/students")
    public StudentRsDto addStudent(@RequestBody StudentRqDto studentRqDto){
        return studentService.addStudent(studentRqDto);
    }

    /**
     * Метод для получения расписания занятий студента.
     * @param id - ID студента.
     * @return возвращает список занятий студента.
     * @see ExerciseRsDto
     */
    @Operation(summary = "Запрос расписания")
    @GetMapping("/students/schedule/{id}")
    public Set<ExerciseRsDto> getSchedule(@Parameter(description = "ID студента") @PathVariable Long id){
        return studentService.getSchedule(id);
    }
}
