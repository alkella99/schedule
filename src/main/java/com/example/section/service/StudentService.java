package com.example.section.service;

import com.example.section.dto.request.StudentRqDto;
import com.example.section.dto.response.ExerciseRsDto;
import com.example.section.dto.response.StudentRsDto;

import java.util.Set;

public interface StudentService {
    StudentRsDto addStudent(StudentRqDto studentRqDto);

    public Set<ExerciseRsDto> getSchedule(Long studentId);
}
