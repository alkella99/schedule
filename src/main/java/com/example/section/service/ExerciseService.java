package com.example.section.service;

import com.example.section.dto.request.AbsentStudentSetRqDto;
import com.example.section.dto.request.ExerciseRqDto;
import com.example.section.dto.response.AbsentStudentSetRsDto;
import com.example.section.dto.response.ExerciseRsDto;

public interface ExerciseService {
    public ExerciseRsDto addExercise(ExerciseRqDto exerciseRqDto) throws Exception;
    public ExerciseRsDto setAbsent(AbsentStudentSetRqDto absentStudentSetRqDto, Long exercise_id) throws Exception;
    public AbsentStudentSetRsDto getAbsent(Long exerciseId) throws Exception;
}
