package com.example.section.service;

import com.example.section.dto.request.TrainerRqDto;
import com.example.section.dto.response.TrainerRsDto;

public interface TrainerService {
    public TrainerRsDto addTrainer(TrainerRqDto trainerRqDto);
}
