package com.example.section.service;

import com.example.section.dto.request.GroupRqDto;
import com.example.section.dto.request.StudentsSetRqDto;
import com.example.section.dto.response.GroupRsDto;

public interface GroupService {
    GroupRsDto addGroup(GroupRqDto groupRqDto) throws RuntimeException;

    public GroupRsDto addStudentsToGroup(StudentsSetRqDto studentsSetRqDto, Long id) throws Exception;

    public GroupRsDto getGroupById(Long id) throws Exception;
}
