package com.example.section.service.impl;

import com.example.section.dto.request.GroupRqDto;
import com.example.section.dto.request.StudentsSetRqDto;
import com.example.section.dto.response.GroupRsDto;
import com.example.section.dto.response.StudentRsDto;
import com.example.section.dto.response.TrainerRsDto;
import com.example.section.entity.Group;
import com.example.section.entity.Student;
import com.example.section.entity.Trainer;
import com.example.section.exception.BadRequestException;
import com.example.section.exception.GroupCapacityExceeded;
import com.example.section.exception.NotFoundRequestException;
import com.example.section.repository.GroupRepository;
import com.example.section.repository.StudentRepository;
import com.example.section.repository.TrainerRepository;
import com.example.section.service.GroupService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private ModelMapper modelMapper;

    public GroupRsDto addGroup(GroupRqDto groupRqDto) throws RuntimeException{
        Group group = new Group();
        Optional<Trainer> trainer = trainerRepository.findById(groupRqDto.getTrainerId());
        if (trainer.isPresent()){
            group.setCapacity(groupRqDto.getCapacity());
            group.setTrainer(trainer.get());
            groupRepository.save(group);
        } else throw new BadRequestException("Тренер не найден.");
        return modelMapper.map(group, GroupRsDto.class);
    }

    public GroupRsDto getGroupById(Long id) throws RuntimeException {
        Group group = groupRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Группа не найдена."));
        GroupRsDto groupRsDto = new GroupRsDto();
        Set<StudentRsDto> studentRsDtos = new HashSet<>();
        group.getStudents().forEach(student -> studentRsDtos.add(modelMapper.map(student, StudentRsDto.class)));
        groupRsDto.setStudents(studentRsDtos);
        groupRsDto.setCapacity(groupRsDto.getCapacity());
        groupRsDto.setTrainer(modelMapper.map(group.getTrainer(), TrainerRsDto.class));
        return groupRsDto;
    }

    public GroupRsDto addStudentsToGroup(StudentsSetRqDto studentsSetRqDto, Long id) throws Exception{
        Group groupFromDB = groupRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("Группа не найдена."));
        if ((groupFromDB.getStudents().size() + studentsSetRqDto.getStudentIdSet().size()) >
                groupFromDB.getCapacity()){
            throw new GroupCapacityExceeded("Размер группы превышен.");
        }
        Set<Long> ids = new HashSet<>();
        Set<Long> idsNotFound = new HashSet<>();
        Set<Student> newStudents = new HashSet<>(studentRepository.
                findAllById(studentsSetRqDto.getStudentIdSet()));
        studentsSetRqDto.getStudentIdSet().forEach(newID -> {
            Student student = studentRepository.findById(newID).orElse(new Student());
            if (student.getFirstName() == null){
                idsNotFound.add(newID);
            }
        });
        if (!idsNotFound.isEmpty()){
            throw new NotFoundRequestException("Студенты со следующими ID не найдены: "+idsNotFound.toString()+".");
        }
        newStudents.forEach(student -> {
            if (!student.getInGroup()) {
                student.setGroup(groupFromDB);
                student.setInGroup(Boolean.TRUE);
            } else {
                ids.add(student.getId());
            }
        });
        if (!ids.isEmpty()){
            throw new BadRequestException("Студенты с ID"+ids.toString()+"уже зачислены в группы.");
        }
        groupFromDB.getStudents().addAll(newStudents);
        groupRepository.save(groupFromDB);
        return modelMapper.map(groupFromDB, GroupRsDto.class);
    }
}
