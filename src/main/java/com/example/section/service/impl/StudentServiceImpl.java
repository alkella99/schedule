package com.example.section.service.impl;

import com.example.section.dto.request.StudentRqDto;
import com.example.section.dto.response.ExerciseRsDto;
import com.example.section.dto.response.StudentRsDto;
import com.example.section.entity.Exercise;
import com.example.section.entity.Student;
import com.example.section.exception.BadRequestException;
import com.example.section.exception.NotFoundRequestException;
import com.example.section.repository.ExerciseRepository;
import com.example.section.repository.StudentRepository;
import com.example.section.service.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private ModelMapper modelMapper;

    public StudentRsDto addStudent(StudentRqDto studentRqDto){
        Student student = Student.builder().build();
        student = modelMapper.map(studentRqDto, Student.class);
        student.setInGroup(Boolean.FALSE);
        return modelMapper.map(studentRepository.save(student), StudentRsDto.class);
    }

    public Set<StudentRsDto> getStudentsNotInGroup(){
        Set<Student> students = studentRepository.getAllByInGroupIsFalse();
        Set<StudentRsDto> studentRsDtoSet = new HashSet<>();
        students.forEach(student -> studentRsDtoSet.add(modelMapper.map(student, StudentRsDto.class)));
        return studentRsDtoSet;
    }

    public Set<ExerciseRsDto> getSchedule(Long studentId){
        Student student = studentRepository.findById(studentId)
                .orElseThrow(()-> new NotFoundRequestException("Студент с данным ID не найден."));
        if (!student.getInGroup()){
            throw new BadRequestException("Студент не назначен в группу.");
        }
        Set<Exercise> exercises = exerciseRepository.findAllExercises(student.getGroup().getId());
        Set<ExerciseRsDto> exerciseRsDtoSet = new HashSet<>();
        exercises.forEach(exercise -> exerciseRsDtoSet.add(modelMapper.map(exercise, ExerciseRsDto.class)));
        if (exerciseRsDtoSet.isEmpty()){
            throw new NotFoundRequestException("Занятия не найдены.");
        }
        return exerciseRsDtoSet;
    }
}
