package com.example.section.service.impl;

import com.example.section.dto.request.AbsentStudentSetRqDto;
import com.example.section.dto.request.ExerciseRqDto;
import com.example.section.dto.response.AbsentStudentSetRsDto;
import com.example.section.dto.response.ExerciseRsDto;
import com.example.section.dto.response.PresenceRsDto;
import com.example.section.entity.Exercise;
import com.example.section.entity.Presence;
import com.example.section.entity.Student;
import com.example.section.exception.BadRequestException;
import com.example.section.exception.NotFoundRequestException;
import com.example.section.repository.ExerciseRepository;
import com.example.section.repository.GroupRepository;
import com.example.section.repository.PresenceRepository;
import com.example.section.repository.StudentRepository;
import com.example.section.service.ExerciseService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Service
public class ExerciseServiceImpl implements ExerciseService {
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private PresenceRepository presenceRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ModelMapper modelMapper;

    public ExerciseRsDto addExercise(ExerciseRqDto exerciseRqDto) throws Exception{
        Set<Exercise> plannedExercises = exerciseRepository.findExerciseByDate();
        exerciseRqDto.setExerciseDate(deleteMinutesAndSeconds(exerciseRqDto));
        plannedExercises.forEach(exercise -> {
            if (exercise.getDate().equals(exerciseRqDto.getExerciseDate())) {
                try {
                    throw new BadRequestException("На это время уже назначено занятие.");
                } catch (BadRequestException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Exercise exercise = new Exercise();
        exercise.setGroup(groupRepository.findById(exerciseRqDto.getGroupId()).get());
        exercise.setDate(exerciseRqDto.getExerciseDate());
        Set<Presence> presence = new HashSet<>();
        exercise.setPresence(presence);
        return modelMapper.map(exerciseRepository.save(exercise), ExerciseRsDto.class);
    }

    public ExerciseRsDto setAbsent(AbsentStudentSetRqDto absentStudentSetRqDto, Long exercise_id) throws Exception{
        Set<Long> absentStudentIds = new HashSet<>();
        Set<Long> markedStudents = new HashSet<>();
        absentStudentSetRqDto.getPresence().forEach(presenceRqDto -> absentStudentIds.add(presenceRqDto.getStudentId()));
        Exercise currentExercise = exerciseRepository.findById(exercise_id)
                .orElseThrow(()-> new NotFoundRequestException("Занятие не найдено"));
        currentExercise.getPresence().forEach(presence -> {
            if (absentStudentIds.contains(presence.getStudent().getId())){
                markedStudents.add(presence.getStudent().getId());
            }
        });
        if (!markedStudents.isEmpty()){
            throw new BadRequestException("Студенты с ID "+markedStudents.toString()+" уже отмечен.");
        }
        Set<Presence> absentStudents = new HashSet<>();
        absentStudentSetRqDto.getPresence().forEach(absentStudent -> {
            Student studentOptional = studentRepository.findById(absentStudent.getStudentId())
                    .orElseThrow(()-> new NotFoundRequestException("Студент с ID "
                            +absentStudent.getStudentId()+" не найден"));
            Presence presence = new Presence();
            presence.setStudent(studentOptional);
            presence.setReason(absentStudent.getReason());
            absentStudents.add(presence);
        });
        absentStudents.forEach(student -> currentExercise.getPresence().add(student));
        return modelMapper.map(exerciseRepository.save(currentExercise), ExerciseRsDto.class);
    }

    public AbsentStudentSetRsDto getAbsent(Long exerciseId) throws Exception{
        Exercise exercise = exerciseRepository.findById(exerciseId)
                .orElseThrow(()-> new NotFoundRequestException("Занятие не найдено"));
        Set<PresenceRsDto> presenceRsDtos = new HashSet<>();
        exercise.getPresence().forEach(presence ->
            presenceRsDtos.add(modelMapper.map(presence, PresenceRsDto.class))
        );
        AbsentStudentSetRsDto absentStudentSetRsDto = new AbsentStudentSetRsDto();
        absentStudentSetRsDto.setAbsentStudents(presenceRsDtos);
        absentStudentSetRsDto.setDate(exercise.getDate());
        absentStudentSetRsDto.setId(exercise.getId());
        return absentStudentSetRsDto;
    }

    private LocalDateTime deleteMinutesAndSeconds(ExerciseRqDto exerciseRqDto){
        return exerciseRqDto.getExerciseDate()
                .minusMinutes(exerciseRqDto.getExerciseDate().getMinute())
                .minusSeconds(exerciseRqDto.getExerciseDate().getSecond())
                .minusNanos(exerciseRqDto.getExerciseDate().getNano());
    }
}
