package com.example.section.service.impl;

import com.example.section.dto.request.TrainerRqDto;
import com.example.section.dto.response.TrainerRsDto;
import com.example.section.entity.Trainer;
import com.example.section.repository.TrainerRepository;
import com.example.section.service.TrainerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainerServiceImpl implements TrainerService {
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private ModelMapper modelMapper;

    public TrainerRsDto addTrainer(TrainerRqDto trainerRqDto){
        ModelMapper modelMapper = new ModelMapper();
        Trainer newTrainer = modelMapper.map(trainerRqDto, Trainer.class);
        return modelMapper.map(trainerRepository.save(newTrainer), TrainerRsDto.class);
    }
}
