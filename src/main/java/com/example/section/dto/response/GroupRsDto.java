package com.example.section.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;
/**
 * Класс для передачи данных клиенту, связанных с добавлением сущности Group.
 * Поля:
 * <b>capacity</b> - виестительность группы.
 * <b>trainer</b> - тренер группы.
 */
@Getter
@Setter
public class GroupRsDto {
    private Long id;
    private Long capacity;
    private Set<StudentRsDto> students;
    private TrainerRsDto trainer;
}
