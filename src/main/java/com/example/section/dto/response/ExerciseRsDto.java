package com.example.section.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Класс для передачи списка отсутствующих и даты занятия
 * <b>date</b> - дата и время занятия
 */
@Getter
@Setter
public class ExerciseRsDto {
    private Long id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;
}
