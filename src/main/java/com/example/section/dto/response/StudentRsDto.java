package com.example.section.dto.response;

import lombok.Getter;
import lombok.Setter;
/**
 * Класс для передачи данных клиенту, связанных с добавлением сущности Student.
 * Поля:
 * <b>id<b/> - ID студента
 * <b>lastName<b/> - фамилия
 * <b>firstName<b/> - имя
 * <b>middleName<b/> - отчество
 * <b>phoneNumber<b/> - номер телефона
 */
@Getter
@Setter
public class StudentRsDto {
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phoneNumber;
}
