package com.example.section.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Класс для передачи данных от клиента, связанных с добавлением сущности Trainer.
 * Поля:
 * <b>lastName<b/> - фамилия
 * <b>firstName<b/> - имя
 * <b>middleName<b/> - отчество
 * <b>phoneNumber<b/> - номер телефона
 */
@Getter
@Setter
@Builder
public class TrainerRqDto {
    @NotNull
    @NotEmpty
    private String lastName;
    @NotNull
    @NotEmpty
    private String firstName;
    @NotNull
    @NotEmpty
    private String middleName;
    @NotNull
    @NotEmpty
    private String phoneNumber;
}
