package com.example.section.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

/**
 * Класс, предназначенный для передачи списка студентов.
 * <b>studentIdSet</b> - множество ID студентов.
 */
@Getter
@Setter
@NoArgsConstructor
public class StudentsSetRqDto implements Serializable {
    Set<Long> studentIdSet;
}
