package com.example.section.dto.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Класс для передачи данных от клиента, связанных с добавлением сущности Group.
 * Поля:
 * <b>capacity</b> - виестительность группы.
 * <b>trainer</b> - тренер группы.
 */
@Getter
@Setter
@Builder
public class GroupRqDto {
    private Long capacity;
    private Long trainerId;
}
