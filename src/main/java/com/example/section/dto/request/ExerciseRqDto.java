package com.example.section.dto.request;

import com.example.section.annotation.ExerciseDateValidation;
import com.example.section.annotation.ExerciseTimeValidation;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Класс для передачи данных от клиента, связанных с добавлением сущности Exercise.
 * Поля:
 * <b>groupId</b> - ID группы, для которой добавляется занятие.
 * <b>exerciseDate</b> - дата и время плнируемого занятия.
 */
@Getter
@Setter
@Builder
public class ExerciseRqDto {
    private Long groupId;
    @ExerciseDateValidation
    @ExerciseTimeValidation
    private LocalDateTime exerciseDate;
}
