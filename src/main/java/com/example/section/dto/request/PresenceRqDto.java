package com.example.section.dto.request;

import lombok.Getter;
import lombok.Setter;

/**
 * Класс сущности Presence. Предназначен для контроля посещаемости занятий.
 * <b>exerciseDate</b> - дата занятия.
 * <b>studentsId</b> - список отсутствующих на занятии.
 */
@Getter
@Setter
public class PresenceRqDto {
    private Long studentId;
    private String reason;
}
