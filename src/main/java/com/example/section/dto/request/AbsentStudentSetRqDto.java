package com.example.section.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class AbsentStudentSetRqDto {
    private Set<PresenceRqDto> presence;
}
