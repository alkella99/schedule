package com.example.section.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
/**
 * Класс сущности Presence. Описывает сущность Presence.
 * Поля:
 * <b>id<b/> - идентификатор
 * <b>student<b/> - список студентов на занятии
 */
@Getter
@Setter
@Entity
@Table(name = "presence")
public class Presence {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "students_id")
    private Student student;
    private String reason;
}
