package com.example.section.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

/**
 * Класс сущности Student. Описывает сущность ученика.
 * Поля: <b>id<b/> - идентификатор, <b>lastName<b/> - фамилия, <b>firstName<b/> - имя, <b>middleName<b/> - отчество,
 * <b>phoneNumber<b/> - номер телефона
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phoneNumber;
    @ManyToOne
    @JoinColumn(name = "groups_id")
    @JsonIgnore
    private Group group;
    private Boolean inGroup;
}
