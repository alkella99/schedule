package com.example.section.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
/**
 * Класс сущности Group. Описывает сущность Группа.
 * Поля:
 * <b>id<b/> - идентификатор
 * <b>capacity<b/> - вместительность
 * <b>trainer<b/> - тренер группы
 * <b>students<b/> - список студентов
 * <b>exercise<b/> - список занятий группы
 */
@Getter
@Setter
@Entity
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long capacity;
    @ManyToOne()
    @JsonIgnore
    private Trainer trainer;
    @OneToMany(mappedBy = "group")
    private Set<Student> students;
    @OneToMany(mappedBy = "group")
    @JsonIgnore
    private Set<Exercise> exercise;
}