package com.example.section.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Класс сущности Exercise. Описывает сущность Занятие.
 * Поля:
 * <b>id<b/> - идентификатор
 * <b>date<b/> - дата занятия
 * <b>group<b/> - группа, у которой проводится занятие
 * <b>presence<b/> - список студентов, присутствующих на занятии
 */
@Getter
@Setter
@Entity
@Table(name = "exercises")
public class Exercise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime date;
    @ManyToOne
    @JoinColumn(name = "groups_id")
    private Group group;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "exercise_id")
    private Set<Presence> presence;
}
