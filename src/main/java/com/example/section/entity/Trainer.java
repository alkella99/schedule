package com.example.section.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * Класс сущности Trainer. Описывает сущность Тренер.
 * Поля: <b>id<b/> - идентификатор
 * <b>lastName<b/> - фамилия
 * <b>firstName<b/> - имя
 * <b>middleName<b/> - отчество
 * <b>phoneNumber<b/> - номер телефона
 */
@Getter
@Setter
@Entity
@Table(name = "trainers")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phoneNumber;
    @OneToMany()
    @JoinColumn(name = "trainer_id")
    private Set<Group> groups;

}
