package com.example.section.controller;

import com.example.section.config.AppTestConfig;
import com.example.section.dto.request.StudentRqDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Класс для тестирования методов StudentController.
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
public class StudentControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AppTestConfig appTestConfig;

    /**
     * Метод тестирования метода createStudent
     * @throws Exception
     */
    @Test
    public void createStudentTest() throws Exception{
        this.mockMvc.perform(appTestConfig.postJson("/students", createStudentRqDto()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName ").value("Ivan"))
                .andExpect(jsonPath("$.middleName ").value("Ivanovich"))
                .andExpect(jsonPath("$.lastName ").value("Sidorov"));
    }

    /**
     * Метод для создания DTO StudentRqDto.
     * @return StudentRqDto
     * @see StudentRqDto
     */
    public StudentRqDto createStudentRqDto() {
        return StudentRqDto.builder()
                .lastName("Sidorov")
                .firstName("Ivan")
                .middleName("Ivanovich")
                .phoneNumber("1234")
                .build();
    }
}
