package com.example.section.controller;

import com.example.section.config.AppTestConfig;
import com.example.section.dto.request.TrainerRqDto;
import com.example.section.repository.TrainerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Класс для тестирования методов TrainerController.
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
public class TrainerControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private AppTestConfig appTestConfig;

    /**
     * Метод для тестирования метода createTrainer
     * @throws Exception
     */
    @Test
    public void createTrainerTest() throws Exception{
        this.mockMvc.perform(appTestConfig.postJson("/trainer", createTrainerRqDto()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.firstName").value("Ivan"))
                .andExpect(jsonPath("$.middleName").value("Ivanovich"))
                .andExpect(jsonPath("$.lastName").value("Ivanov"));
    }

    /**
     * Метод для создания DTO TrainerRqDro.
     * @return TrainerRqDro.
     * @see TrainerRqDto
     */
    public TrainerRqDto createTrainerRqDto() {
        return TrainerRqDto.builder()
                .lastName("Ivanov")
                .firstName("Ivan")
                .middleName("Ivanovich")
                .phoneNumber("1234")
                .build();
    }

}
