package com.example.section.controller;

import com.example.section.config.AppTestConfig;
import com.example.section.dataset.GroupDataSet;
import com.example.section.dto.request.ExerciseRqDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.Month;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Класс для тестирования методов контроллера ExerciseController
 * @see ExerciseController
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
public class ExerciseControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AppTestConfig appTestConfig;
    @Autowired
    private GroupDataSet groupDataSet;
    @BeforeEach
    public void setUp() throws Exception{
        groupDataSet.createData();
    }

    /**
     * Тестирование метода addExercise.
     * @throws Exception
     */
    @Test
    public void addExerciseTest() throws Exception {
        this.mockMvc.perform(appTestConfig.postJson("/exercise", createExerciseRqDto()))
                .andExpect(status().isOk());
    }

    /**
     * Методля для создания DTO ExerciseRqDto
     * @see ExerciseRqDto
     * @return возвращает DTO класса ExerciseRqDto
     */
    private ExerciseRqDto createExerciseRqDto(){
        LocalDateTime testDate = LocalDateTime.of(2222, Month.JANUARY, 11, 15, 00, 00);
        return ExerciseRqDto.builder().exerciseDate(testDate).groupId(1L).build();
    }
}
