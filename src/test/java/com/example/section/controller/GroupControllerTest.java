package com.example.section.controller;

import com.example.section.config.AppTestConfig;
import com.example.section.dataset.ExerciseDataSet;
import com.example.section.dataset.GroupDataSet;
import com.example.section.dataset.StudentDataSet;
import com.example.section.dto.request.GroupRqDto;
import com.example.section.dto.request.StudentsSetRqDto;
import com.example.section.repository.GroupRepository;
import com.example.section.repository.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Класс для тестирования методов контроллера GroupController
 * @see GroupController
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test"})
public class GroupControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupDataSet groupDataSet;
    @Autowired
    private StudentDataSet studentDataSet;
    @Autowired
    private ExerciseDataSet exerciseDataSet;
    @Autowired
    private AppTestConfig appTestConfig;
    @Autowired
    private StudentRepository studentRepository;
    @BeforeEach
    public void setUp() throws Exception {
        groupDataSet.createData();
        studentDataSet.createData();
    }
    @AfterEach
    public void dropData(){
        exerciseDataSet.deleteData();
        studentDataSet.deleteData();
        groupDataSet.deleteData();
    }

    /**
     * Метод для тестирования метода createGroup.
     * @throws Exception
     */
    @Test
    public void createGroup() throws Exception {
        this.mockMvc.perform(appTestConfig.postJson("/group", createGroupRqDto()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.capacity").value(10));
    }
    /**
     * Метод для тестирования метода addStudent.
     * @throws Exception
     */
    @Test
    public void addStudents() throws Exception{
        Long id = groupRepository.findTopByOrderByIdDesc().getId();
        this.mockMvc.perform(appTestConfig.putJson("/group/", studentsListRqDto(), id.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.students").exists());
    }
    /**
     * Метод для тестирования метода getGroup.
     * @throws Exception
     */
    @Test
    public void getGroup() throws Exception{
        Long id = groupRepository.findTopByOrderByIdDesc().getId();
        this.mockMvc.perform(appTestConfig.getJson("/group/", id.toString()))
                .andExpect(status().isOk());
    }
    /**
     * Метод для создания DTO GroupRqDto.
     * @see GroupRqDto
     * @throws Exception
     */
    private GroupRqDto createGroupRqDto(){
        return GroupRqDto.builder()
                .trainerId(1L)
                .capacity(10L)
                .build();
    }
    /**
     * Метод для создания DTO StudentsListRqDto.
     * @see StudentsSetRqDto
     * @throws Exception
     */
    private StudentsSetRqDto studentsListRqDto(){
        Set<Long> studentIds = new HashSet<>();
        studentRepository.findAll().forEach(student -> studentIds.add(student.getId()));
        StudentsSetRqDto studentsSetRqDto = new StudentsSetRqDto();
        studentsSetRqDto.setStudentIdSet(studentIds);
        return studentsSetRqDto;
    }
}
