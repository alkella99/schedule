package com.example.section.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Вспомогательный класс для создания методов запросов.
 */
@Configuration
public class AppTestConfig {
    /**
     * Метод для формирования POST-запроса
     * @param uri - ссылка метода
     * @param body - DTO
     * @return Возвращает объект класса MockHttpServletRequestBuilder, который передается в запрос
     */
    public static MockHttpServletRequestBuilder postJson(String uri, Object body) {
        try {
            ObjectMapper om = new ObjectMapper();
            om.registerModule(new JavaTimeModule());
            String json = om.writeValueAsString(body);
            return post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * Метод для формирования PUT-запроса
     * @param uri - ссылка метода
     * @param body - DTO
     * @param param - параметр, передаваемый в запрос
     * @return Возвращает объект класса MockHttpServletRequestBuilder, который передается в запрос
     */
    public static MockHttpServletRequestBuilder putJson(String uri, Object body, String param){
        try {
            String json = new ObjectMapper().writeValueAsString(body);
            return put(uri+param)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .queryParam("id", param)
                    .content(json);
        } catch (JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Метод для формирования GET-запроса
     * @param uri - ссылка метода
     * @param param - параметр, передаваемый в запрос
     * @return Возвращает объект класса MockHttpServletRequestBuilder, который передается в запрос
     */
    public static MockHttpServletRequestBuilder getJson(String uri, String param){
        return get(uri+param)
                .queryParam("id", param);
    }
}
