package com.example.section.dataset;

public interface ExerciseDataSet {
    void createData() throws Exception;
    void deleteData();
}
