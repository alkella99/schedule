package com.example.section.dataset;

public interface TrainerDataSet {
    void createData() throws Exception;
    void deleteData();
}
