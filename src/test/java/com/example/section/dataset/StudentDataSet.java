package com.example.section.dataset;

public interface StudentDataSet {
    void createData();
    void deleteData();
}
