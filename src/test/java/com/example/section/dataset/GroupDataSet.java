package com.example.section.dataset;

public interface GroupDataSet {
    void createData() throws Exception;
    void deleteData();
}
