package com.example.section.dataset.impl;

import com.example.section.dataset.GroupDataSet;
import com.example.section.dto.request.TrainerRqDto;
import com.example.section.entity.Group;
import com.example.section.entity.Trainer;
import com.example.section.repository.GroupRepository;
import com.example.section.repository.TrainerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GroupDataSetImpl implements GroupDataSet {
    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    @Transactional
    public void createData() throws Exception {
        createGroup();
    }

    @Override
    public void deleteData() {
        groupRepository.deleteAllInBatch();
        trainerRepository.deleteAllInBatch();
    }

    public Trainer createTrainer(TrainerRqDto trainerRqDto){
        Trainer trainer = new Trainer();
        trainer = mapper.map(trainerRqDto, Trainer.class);
        return trainerRepository.save(trainer);
    }

    public void createGroup(){
        Group group = new Group();
        group.setTrainer(createTrainer(createTrainerRqDto()));
        group.setCapacity(10L);
        groupRepository.save(group);
    }

    public TrainerRqDto createTrainerRqDto(){
        return TrainerRqDto.builder()
                .lastName("Ivanov")
                .firstName("Ivan")
                .middleName("Ivanovich")
                .phoneNumber("1234")
                .build();
    }
}
