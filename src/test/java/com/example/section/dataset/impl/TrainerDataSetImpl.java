package com.example.section.dataset.impl;

import com.example.section.dataset.TrainerDataSet;
import com.example.section.dto.request.TrainerRqDto;
import com.example.section.repository.TrainerRepository;
import com.example.section.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;

public class TrainerDataSetImpl implements TrainerDataSet {
    @Autowired
    private TrainerService trainerService;
    @Autowired
    private TrainerRepository trainerRepository;

    @Override
    public void createData() throws Exception {
        createTrainer(createTrainerRqDto());
    }

    @Override
    public void deleteData() {
        trainerRepository.deleteAllInBatch();
    }

    public void createTrainer(TrainerRqDto trainerRqDto){
        trainerService.addTrainer(trainerRqDto);
    }

    public TrainerRqDto createTrainerRqDto(){
        return TrainerRqDto.builder()
                .lastName("Ivanov")
                .firstName("Ivan")
                .middleName("Ivanovich")
                .phoneNumber("1234")
                .build();
    }
}
