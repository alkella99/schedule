package com.example.section.dataset.impl;

import com.example.section.dataset.StudentDataSet;
import com.example.section.entity.Group;
import com.example.section.entity.Student;
import com.example.section.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class StudentDataSetImpl implements StudentDataSet {
    @Override
    public void createData() {
        deleteData();
        createStudents();
    }

    @Override
    public void deleteData() {
        studentRepository.deleteAllInBatch();
    }

    @Autowired
    private StudentRepository studentRepository;

    public void createStudents(){
        Set<Student> students = new HashSet<>();
        Group group = new Group();
        group.setId(1L);
        students.add(Student.builder()
                .lastName("Petrov")
                .firstName("Petr")
                .middleName("Petrovich")
                .phoneNumber("1234")
                .inGroup(Boolean.FALSE)
                .build());
        students.add(Student.builder()
                .lastName("Mihaylov")
                .firstName("Mihail")
                .middleName("Mihaylovich")
                .phoneNumber("1234")
                .inGroup(Boolean.FALSE)
                .build());
        students.add(Student.builder()
                .lastName("Sergeev")
                .firstName("Sergey")
                .middleName("Sergeevich")
                .phoneNumber("1234")
                .inGroup(Boolean.FALSE)
                .build());
        studentRepository.saveAll(students);
    }
}
