package com.example.section.dataset.impl;

import com.example.section.dataset.ExerciseDataSet;
import com.example.section.repository.ExerciseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExerciseDataImpl implements ExerciseDataSet {
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Override
    public void createData() throws Exception {

    }

    @Override
    public void deleteData() {
        dropData();
    }

    public void dropData(){
        exerciseRepository.deleteAllInBatch();
    }
}
